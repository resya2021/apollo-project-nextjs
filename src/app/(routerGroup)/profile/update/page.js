import FormUpdate from '@/components/profile/form/form.components';
import 'bootstrap/dist/css/bootstrap.css';
import './page.styles.css';
import { cookies } from 'next/headers';

export default function ProfileUpdate() {
  const cookieStore = cookies();
  const userId = cookieStore.get('userId').value;
  return (
    <>
      <div className="profile">
        <div className="row justify-content-center">
          <div className="col-lg-4">
            <h1 className="text-center mt-5 mb-5">User Profile</h1>
            <FormUpdate userId={userId} />
          </div>
        </div>
      </div>
    </>
  );
}
